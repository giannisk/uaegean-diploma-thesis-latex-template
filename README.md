# UAegean Diploma Thesis LaTeX Template

This is an all-inclusive LaTeX template for diploma theses at the University of the Aegean.

![Template Preview](preview/preview.png)

[PDF Preview](preview/preview.pdf)

Based on the [TUM Bachelor/Master Thesis LaTeX Template](https://github.com/fwalch/tum-thesis-latex).

## Quickstart Guide

1. Edit `main.tex` and `settings.tex`. Pay particular attention to the TODO comments.
2. Add chapters, appendices, bibliography entries and relevant content.
3. Run `xelatex main`, `biber main` , `xelatex main` and `xelatex main` in this specific order.
4. Check the generated PDF file.

## License

Final work is made available under [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/). This license only applies to the template; there are no restrictions on the resulting PDF file or the contents of the thesis.

The TUM Bachelor/Master Thesis LaTeX Template is available under [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).

## Disclaimer

This LaTeX template is neither officially approved nor endorsed by the University of the Aegean. It is important to consult with your supervisor prior to using this template.
